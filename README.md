# sis8300llrf conda recipe

Home: https://gitlab.esss.lu.se/epics-modules/sis8300llrf

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS sis8300llrf module
